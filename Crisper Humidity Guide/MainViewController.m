//
//  MainViewController.m
//  Crisper Humidity Guide
//
//  Created by Mark Rudolph on 12/10/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize humidityValue;
@synthesize timer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if(&UIApplicationWillEnterForegroundNotification) {
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(appResumed:) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.delegate.myDrone.dronePeripheral state] == CBPeripheralStateConnected ) {
        
        NSLog(@"Sensordrone is connected! Start measuring...");
        if (!self.delegate.myDrone.humidityStatus) {
            [self.delegate.myDrone enableHumidity];
        }
        
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self.delegate.myDrone selector:@selector(measureHumidity) userInfo:nil repeats:YES];
        
    } else {
        NSLog(@"Sensordrone not connected...");
        if (timer != nil) {
            NSLog(@"Invalidating timer...");
            [timer invalidate];
            timer = nil;
        }
    }
}

-(void)viewDidDisappear:(BOOL)animated {
    // Invalidate the timer
    if (timer != nil) {
        NSLog(@"Invalidating timer...");
        [timer invalidate];
        timer = nil;
    }
    [humidityValue setText:@"--"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)doOnHumidityEnabled {
    NSLog(@"Humidity enabled...");
    [self.delegate.myDrone measureHumidity];
}

-(void)doOnHumidityMeasured {
    NSLog(@"Humidity measured...");
    [humidityValue setText:[NSString stringWithFormat:@"%.0f %%", self.delegate.myDrone.measuredPercentHumidity]];
    
}

- (IBAction)showInfoHigh:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"High Humidty: (90% RH+)" message:@"Appropriate for crisp, leafy greens and most vegetables. Examples: Asparagus, apples, beets, broccoli, brussel sprouts, cabbage, carrots, cauliflower, celeriac, celery, collards, corn, endive, grapes, kale, leeks, parsley, parsnips, pears, peas, radishes, rhubarb, rutabagas, spinach" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

- (IBAction)showInfoMedium:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Medium Humidty: (75%-90% RH)" message:@"Appropriate for thin skinned fruits and vegetables. Examples: Beans, cucumbers, eggplant, cantaloupe, watermelon, peppers, potatoes, tomatoes." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

- (IBAction)showInfoLow:(id)sender {
    [[[UIAlertView alloc] initWithTitle:@"Low Humidty: (<70% RH)" message:@"Appropriate for thick skinned, layered produce. Examples: Garlic, onions." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

-(void) doOnConnectionLost {
    [self.bleStatusIcon showDisconnected];
    [self.delegate showConnectionLostDialog];
    [humidityValue setText:@"--"];
    
}

-(void)appResumed:(NSNotification *)notification {
    if (
        [self.delegate.myDrone.dronePeripheral state] == CBPeripheralStateConnected
        && [self.delegate.myDrone delegate ] == self)
    {
        // This is how you'd do it
    }
}
@end
