//
//  MainViewController.h
//  Crisper Humidity Guide
//
//  Created by Mark Rudolph on 12/10/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"

@interface MainViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UILabel *humidityValue;
@property NSTimer *timer;


- (IBAction)showInfoHigh:(id)sender;
- (IBAction)showInfoMedium:(id)sender;
- (IBAction)showInfoLow:(id)sender;

-(void)appResumed:(NSNotification*) notification;

@end
