//
//  InfoViewController.h
//  Crisper Humidity Guide
//
//  Created by Mark Rudolph on 12/11/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"

@interface InfoViewController : BaseViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *infoView;

@end
